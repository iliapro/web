import './Main.css'
import {NavLink, useParams} from "react-router-dom"
import {useEffect, useRef, useState} from "react"
import texts from "../../texts"

const Main = () => {
    const {lang} = useParams()
    const topImageElem = useRef()

    const [imageTop, setImageTop] = useState(0)
    const [mouseRotateX, setMouseRotateX] = useState(0)
    const [mouseRotateY, setMouseRotateY] = useState(0)
    const [cubeSideY, setCubeSideY] = useState(0)
    const [sideNumber, setSideNumber] = useState(0)

    const onScroll = e => {
        if (e.target.documentElement.scrollTop / 3 < topImageElem.current.clientHeight - 70)
            setImageTop(e.target.documentElement.scrollTop / 3)
    }

    const moveCude = e => {
        setMouseRotateY((e.x / window.innerWidth) * 24 - 12)
        setMouseRotateX(-(e.y / window.innerHeight) * 24 + 12)
    }

    useEffect(() => {
        window.addEventListener('scroll', onScroll)
        window.addEventListener('mousemove', moveCude)
    }, []);

    useEffect(() => {
        setCubeSideY(sideNumber % 4 * (-90))
    }, [sideNumber])

    return (
        <div className={'Main'}>
            <header className={'MainHeader'}>
                <div className={'MainHeaderLogo'}/>
                <nav className={'MainHeaderNav'}>
                    <a className={'MainHeaderButtons'}>{texts.nav.main[lang]}</a>
                    <a className={'MainHeaderButtons'}>{texts.nav.aboutUs[lang]}</a>
                    <a className={'MainHeaderButtons'}>{texts.nav.services[lang]}</a>
                    <a className={'MainHeaderButtons'}>{texts.nav.contacts[lang]}</a>
                </nav>
                <div className={'MainHeaderLangContainer'}>
                    <NavLink className={'MainHeaderLang'} to={'/ru'}>ru</NavLink>
                    <NavLink className={'MainHeaderLang'} to={'/en'}>en</NavLink>
                </div>
            </header>
            <div className={'MainTopImage'} style={{backgroundPositionY: `${imageTop}px`}} ref={topImageElem}>
                <div className={'MainTopImageInside'}>
                    <p className={'MainTopImageInsideText'}>
                        {texts.topImage.text[lang]}
                    </p>
                    <div className={'MainTopImageInsideButtonsContainer'}>
                        <a href={'tel:+79115501508'}
                           className={'MainTopImageInsideButtons'}>{texts.topImage.buttons.call[lang]}</a>
                        <a href={'mailto:test@test.test'}
                           className={'MainTopImageInsideButtons'}>{texts.topImage.buttons.mail[lang]}</a>
                    </div>
                </div>
            </div>
            <main className={'MainContent'}>
                <div className={'MainContentFeatures'}>
                    <div className={'MainContentFeatureItem'}>
                        <p className={'MainContentFeatureItemTitle'}>{texts.features[0].title[lang]}</p>
                        <div className={'MainContentFeatureItemImg Zero'}>
                            <p className={'MainContentFeatureItemText'}>{texts.features[0].text[lang]}</p>
                        </div>
                    </div>
                    <div className={'MainContentFeatureItem'}>
                        <p className={'MainContentFeatureItemTitle'}>{texts.features[1].title[lang]}</p>
                        <div className={'MainContentFeatureItemImg One'}>
                            <p className={'MainContentFeatureItemText'}>{texts.features[1].text[lang]}</p>
                        </div>
                    </div>
                    <div className={'MainContentFeatureItem'}>
                        <p className={'MainContentFeatureItemTitle'}>{texts.features[2].title[lang]}</p>
                        <div className={'MainContentFeatureItemImg Two'}>
                            <p className={'MainContentFeatureItemText'}>{texts.features[2].text[lang]}</p>
                        </div>
                    </div>
                </div>
                <p className={'MainServicesLabel'}>{texts.services.label[lang]}</p>
                <div className={'MainServices'}>
                    <div className={'MainServicesContainer'}>
                        <div className={'MainServicesLeft'}>
                            <p className={'MainServicesLeftServiceName'}
                               style={{marginTop: 0}}
                               onClick={() => setSideNumber(0)}>• {texts.services[0].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(1)}>• {texts.services[1].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(2)}>• {texts.services[2].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(3)}>• {texts.services[3].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(4)}>• {texts.services[4].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(5)}>• {texts.services[5].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(6)}>• {texts.services[6].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(7)}>• {texts.services[7].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(8)}>• {texts.services[8].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(9)}>• {texts.services[9].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(10)}>• {texts.services[10].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(11)}>• {texts.services[11].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(12)}>• {texts.services[12].title[lang]}</p>
                            <p className={'MainServicesLeftServiceName'}
                               onClick={() => setSideNumber(13)}>• {texts.services[13].title[lang]}</p>
                        </div>
                        <div className={'MainServicesRight'}>
                            <p className={'MainServicesRightServiceName'}>{texts.services[sideNumber].title[lang]}</p>
                            <div className={'CubeContainer'} style={{
                                transform: `rotateX(${mouseRotateX}deg) 
                                rotateY(${mouseRotateY}deg)`
                            }}>
                                <div className="cube" style={{
                                    transform: `rotateY(${cubeSideY}deg)`
                                }}>
                                    <div className="side side1"/>
                                    <div className="side side2"/>
                                    <div className="side side3"/>
                                    <div className="side side4"/>
                                    <div className="side side5"/>
                                    <div className="side side6"/>
                                </div>
                            </div>
                            <p className={'MainServicesRightServiceDescription'}>
                                {texts.services[sideNumber].text[lang]}
                            </p>
                        </div>
                    </div>
                </div>
            </main>
            <div className={'space'}/>
        </div>
    )
}

export default Main