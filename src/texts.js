const texts = {
    nav: {
        main: {
            ru: 'Главная',
            en: 'Main'
        },
        aboutUs: {
            ru: 'О нас',
            en: 'About us'
        },
        services: {
            ru: 'Сервисы',
            en: 'Services'
        },
        contacts: {
            ru: 'Контакты',
            en: 'Contacts'
        }
    },
    topImage: {
        text: {
            ru: 'Мы специализируемся на консультационных услугах в сфере судоходства',
            en: 'We specialize in compliance-related shipping consultancy services'
        },
        buttons: {
            call: {
                ru: 'Позвонить',
                en: 'Call'
            },
            mail: {
                ru: 'Написать',
                en: 'Mail'
            }
        }
    },
    features: {
        0: {
            title: {
                ru: 'Мы - надёжность',
                en: 'We are reliability'
            },
            text: {
                ru: 'Каждый день мы помагаем сотням наших клиентов оставаться наплаву)))',
                en: 'Every day we help hundreds of our clients stay afloat'
            }
        },
        1: {
            title: {
                ru: 'Мы крутые',
                en: 'We are reliability'
            },
            text: {
                ru: 'Мы настолько крутые, что это ваще круто. И с нами работают только самые крутые, это позволяет нам оставваться самыми крутыми среди крутых',
                en: 'Every day we help hundreds of our clients stay afloat'
            }
        },
        2: {
            title: {
                ru: 'Очень',
                en: 'We are reliability'
            },
            text: {
                ru: 'Очень-преочень крутые. ',
                en: 'Every day we help hundreds of our clients stay afloat'
            }
        }
    },
    services: {
        label: {
            ru: 'Наши сервисы',
            en: 'Our services'
        },
        0: {
            title: {
                ru: 'Портовый капитан/суперитендант по грузу',
                en: 'Port captain/cargo superintendent'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        1: {
            title: {
                ru: 'Pre-sire/CDI & pre-ovid веттинг-инспекции',
                en: 'Pre-sire/CDI & pre-ovid vetting inspection'
            },
            text: {
                ru: 'Мы размещаем нашего морского консультанта на борту судна перед проверкой для подготовки судна и персонала судна в соответствии с последними основными требованиями SIRE VIQ, OVIQ, CDI, АО РОСНЕФТЕФЛОТ и требованиями нефтяных компаний. После проведения предварительной проверки Владельцу/Менеджеру для изучения предоставляется отчет по наблюдениям с предложениями по устранению выявленных наблюдений. Оценка риска, подготовленная нашей компанией, используется для предварительной проверки судна, и мы рекомендуем Владельцам / Менеджерам проводить внешнею веттинг-инспекцию только после того, как судно показало хорошие результаты согласно нашей оценки риска. ООО «Марин Плюс» также может оказать помощь в подготовки и сопровождения полного веттинг процесса (подготовка, оформление, организация, проведение, устранение и последующий контроль), как веттинг суперинтендант. После проверки мы помогаем выполнить анализ первопричины любого наблюдения и подготовить хороший ответ.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        2: {
            title: {
                ru: 'Навигационный аудиты',
                en: 'Navigational audits'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        3: {
            title: {
                ru: 'Внутренний аудит по МКУБ',
                en: 'Internal ISM audit'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        4: {
            title: {
                ru: 'Внутренний аудит по ОСПС судна',
                en: 'Internal ISPS audit (ship)'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        5: {
            title: {
                ru: 'Аудит на соответствие КТМС',
                en: 'Maritime labour convention verification audit'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        6: {
            title: {
                ru: 'Аудит на соответствие требованиям государственного портового контроля',
                en: 'Port state control verification audit'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        7: {
            title: {
                ru: 'Инспекции судов/яхт/оффшорных установок перед продажей и покупкой',
                en: 'Pre-purchase and pre-sale vessel/yacht/offshore installation inspections'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        8: {
            title: {
                ru: 'TMSA/OVMSA аудит',
                en: 'TMSA/OVMSA audit'
            },
            text: {
                ru: 'Мы размещаем нашего морского консультанта на борту судна перед проверкой для подготовки судна и персонала судна в соответствии с последними основными требованиями SIRE VIQ, OVIQ, CDI, АО РОСНЕФТЕФЛОТ и требованиями нефтяных компаний. После проведения предварительной проверки Владельцу/Менеджеру для изучения предоставляется отчет по наблюдениям с предложениями по устранению выявленных наблюдений. Оценка риска, подготовленная нашей компанией, используется для предварительной проверки судна, и мы рекомендуем Владельцам / Менеджерам проводить внешнею веттинг-инспекцию только после того, как судно показало хорошие результаты согласно нашей оценки риска. ООО «Марин Плюс» также может оказать помощь в подготовки и сопровождения полного веттинг процесса (подготовка, оформление, организация, проведение, устранение и последующий контроль), как веттинг суперинтендант. После проверки мы помогаем выполнить анализ первопричины любого наблюдения и подготовить хороший ответ.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        9: {
            title: {
                ru: 'Расследование инцидентов',
                en: 'Incident investigation'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        10: {
            title: {
                ru: 'Оценка готовности к чрезвычайным ситуациям',
                en: 'Emergency readiness assessments'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        11: {
            title: {
                ru: 'Необъявленные проверки на алкоголь и наркотики',
                en: 'Unnounced drug test service'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        12: {
            title: {
                ru: 'Независимый технический аудит',
                en: 'Independent technical audit'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        },
        13: {
            title: {
                ru: 'Аудит кадровых агенств',
                en: 'Manning agent audits'
            },
            text: {
                ru: 'Мы предоставляем Владельцам / Фрахтователям полный спектр услуг портового Капитана / Суперинтенданта по грузу, который поможет Капитану во всех аспектах грузовых операций (сыпучие груза, контейнера, жидкие груза и химические груза) и подготовки грузовых танков /грузовых трюмов к погрузке. После назначения наш Капитан/Суперинтендант по грузу тесно взаимодействует со всеми сторонами, чтобы обеспечить предварительное планирование грузовых операций и обеспечить профессиональную погрузку и / или выгрузку. Во время грузовых операций наш портовый Капитан/Суперинтендант по грузу присутствует круглосуточно. На сегодняшний день данный сервис осуществляется в Российских портах и на рейдах Балтийского моря, Черного, Азовского и Каспийского морей.',
                en: 'We provide Owners/Charterers a Full range of Port Captain/Cargo Superintendent services to assist Master in all facets of cargo operations (Dry, Containerized, Liquid & Chemical) and also for Bunker Surveys. Once appointed our Port Captain liaises closely with all parties to ensure pre-planning of the cargo operations and ensure a professional load and/or discharge operation. During cargo operations we have our Port captain in attendance on a 24/7 basis. Presently this service is carried out in the Russian ports and on raids of the Baltic sea, the Black, Azov and Caspian seas.'
            }
        }
    }
}

export default texts
